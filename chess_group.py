import sys

CHESS_BOARD = [['A8', 'B8', 'C8', 'D8', 'E8', 'F8', 'G8', 'H8'],
               ['A7', 'B7', 'C7', 'D7', 'E7', 'F7', 'G7', 'H7'],
               ['A6', 'B6', 'C6', 'D6', 'E6', 'F6', 'G6', 'H6'],
               ['A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5'],
               ['A4', 'B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4'],
               ['A3', 'B3', 'C3', 'D3', 'E3', 'F3', 'G3', 'H3'],
               ['A2', 'B2', 'C2', 'D2', 'E2', 'F2', 'G2', 'H2'],
               ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1']]

def positions_list(ranks :int , files : int) -> list[str]:
    rows = r = i = ranks
    columns = c = j = files  
    pos_list =[]             
    while ranks <= len(CHESS_BOARD)-1 and files <= len(CHESS_BOARD)-1 :
        pos_list.append(CHESS_BOARD[ranks][files])
        ranks+= 1
        files+= 1
    while rows >= 0 and columns >= 0:
        pos_list.append(CHESS_BOARD[rows][columns])
        rows-= 1
        columns-= 1
    while r <= len(CHESS_BOARD)-1 and c >= 0:
        pos_list.append(CHESS_BOARD[r][c])
        r+= 1
        c-= 1
    while i >= 0 and j <= len(CHESS_BOARD)-1:
        pos_list.append(CHESS_BOARD[i][j])
        i-= 1
        j+= 1
    return list(set(pos_list))

def bishop_movements(current_pos : str) -> list[str]:
    pos_list = []
    (ranks, files) = current_place(current_pos)
    return positions_list(ranks ,files)

def rook_movements(current_pos: str) -> list:
    rank_val = current_pos[1]
    file_val = current_pos[0]
    possible_targets = []
    for rank in CHESS_BOARD:
        if rank[0][1] == rank_val:
            possible_targets.extend(rank)
    pos = ord(file_val) - 65
    for file in CHESS_BOARD:
        possible_targets.append(file[pos])
    return sorted(list(set(possible_targets) - {current_pos}))

def current_place(pos : str) -> tuple:
    ranks, files = 0, 0
    for rank in CHESS_BOARD:
        if pos in rank:
            ranks = CHESS_BOARD.index(rank)
            files = CHESS_BOARD[ranks].index(pos)
            return ranks, files    

def is_valid(square : tuple) -> bool:
    try:
        return square[0] in range(8) and square[1] in range(8)
    except IndexError:
        return False

def knight_movements(pos : str):
    (rank, file) = current_place(pos)
    step = [-2, 2,-1, 1]
    moves = []
    for r in step:
        for f in step:
            #print(CHESS_BOARD[rank + r] [file + f])
            if abs(f) != abs(r) and is_valid((rank + r, file + f)):        
                moves.append(CHESS_BOARD[rank + r] [file + f])
    return moves

def queen_movements(current_pos : str) ->list:
    return list(set(rook_movements(current_pos) + bishop_movements(current_pos)))

def cut_over(piece1: str, pos1: str, piece2: str, pos2: str) -> str:
    pieces = ["rook", "knight", "queen", "bishop"]
    piece1_fname = [rook_movements(pos1), knight_movements(pos1), queen_movements(pos1), bishop_movements(pos1)]
    piece2_fname = [rook_movements(pos2), knight_movements(pos2), queen_movements(pos2), bishop_movements(pos2)]
    for i in range(len(pieces)):
        if piece1 == pieces[i]:
            piece1_movements = piece1_fname[i]
        if piece2 == pieces[i]:
            piece2_movements = piece2_fname[i]      
    if pos1 in piece2_movements:
        return piece2 + " cuts " + piece1
    elif pos2 in piece1_movements:
        return piece1 + " cuts " + piece2
    else:
        return "Can't cut"
    
for arg in sys.argv[1:]:
    piece1, pos1, piece2, pos2 = map(str.strip, arg.split(','))  
    print(cut_over(piece1, pos1, piece2, pos2 ))

