This repository has my take on the given problem.

1. modularinverse.py  - this program contains function to find the inverse of a number say A in the world of number B using euclid extended algorithm.

2. decryption.py - this program contains set of function which can read the file and give the frequency of characters or string to make decryption easier.
